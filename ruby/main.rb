require 'json'
require 'socket'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

class RaceInfo

  def initialize(race)
    @trackInfoString = race['track']['id'] + " : " + race['track']['name']
    @cars = race['cars']
    @pieces = race['track']['pieces']
    @lanes = race['track']['lanes']
    @laps = race['raceSession']['laps']
    @maxLapTime = race['raceSession']['maxLapTimeMs']
    @startingPoint = race['track']['startingPoint']
  end

  def print_arr(arr)
    puts ""
    i = 0
    arr.each{|elem|
      puts i.to_s + "  " + elem.to_s
      i += 1
    }
  end

  def get_pieces
    return @pieces
  end

  def get_lanes
    return @lanes
  end

  def get_laps
    return @laps
  end

  def get_maxLapTime
    return @maxLapTime
  end

  def get_startingPoint
    return @startingPoint
  end

  def get_cars
    return @cars
  end

  def trackInfo
    puts @trackInfoString
  end
end

class NoobBot
  def initialize(server_host, server_port, bot_name, bot_key)
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)
    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    @speedLimit_1 = 200
    @speedLimit_2 = 300
    @speedLimit_3 = 400
    @switch_done = start_flag = true
    @turbo = @turbo_on = @incremented = ok = @start_count = false
    @lambda = piece = speed = angle = nextTurn = reqSpeed= delta = needed_dist = @count_on = distance = @angle_max =  @old_speed = @dist = start_speed = end_speed =0
    sp = 0.5

    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      case msgType

        when 'carPositions'
          if piece != msgData[0]['piecePosition']['pieceIndex']

            puts msgData[0]['piecePosition']['pieceIndex'].to_s + "  :  " + speed.to_s + "  :  " + sp.to_s

            @incremented = false

            if @count_on > 3
              @turbo_on = @start_count =false 
              @count_on = 0
              @start_count = false
            end

            @count_on += 1 if @start_count
          end

          speed, piece, angle, lap = read_info(msgData)

          @angle_max = @angle_max < angle ? angle : @angle_max  
          increase_limit(piece) if piece > nextTurn && speed > reqSpeed-10

          distance, delta, reqSpeed, nextTurn = get_distance(msgData, piece, speed, lap, tcp)

          if delta > 550 && @turbo  
            tcp.puts turbo_message
            @turbo = false
            @turbo_on = true
          end

          if distance > 0 || speed < reqSpeed
            sp = 1.0
          else 
            sp = 0.0
          end

          sp = 0.0 if @start_count && speed > 500
          
          tcp.puts throttle_message(sp)

        when 'gameInit'
          @gameInfo = RaceInfo.new(msgData['race'])
          @pieces = @gameInfo.get_pieces
          @len = @pieces.length
          @shape, @turns = track_shape(@pieces)
          @lengths_arr = get_lengths(@pieces)

        else
          case msgType
            when 'join'
              puts 'Joined'
            when 'gameStart'
              puts 'Race started'
            when 'crash'
              puts 'Someone crashed'
            when 'gameEnd'
              puts 'Race ended'
            when 'error'
              puts "ERROR: #{msgData}"
            when 'turboAvailable'
              @turbo = true
            when 'turboEnd'
              @count_on = 0
              @start_count = true
          end
          puts "Got #{msgType}"
          tcp.puts ping_message
      end
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def create_race(trackName, carCount, bot_name, bot_key, pass)
    return JSON.generate({:msgType => "createRace", :data => {:botId => {:name =>bot_name, :key =>bot_key}, 
      :trackName => trackName, :carCount => carCount}})
  end

  def join_race(trackName, carCount, bot_name, bot_key, pass)
    JSON.generate({:msgType => "joinRace", :data => {:botId => {:name =>bot_name, :key =>bot_key}, 
      :trackName => trackName, :carCount => carCount}})
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def turbo_message
    make_msg("turbo", "Team rocket blasting off again!")
  end

  def switch_message(direction)
    make_msg("switch", direction)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end

  def track_shape(pieces)
    m = k = pie = angle =0
    shape = Array.new
    turns = Array.new

    pieces.each{|piece|

      if piece.has_key?('length')
        pie = k = angle = 0
        shape.push(k)
      elsif pie != piece['radius']
        k +=1
        shape.push(k)
        pie = piece['radius']
        angle += piece['angle']
      else 
        if angle * piece['angle'] < 0
          angle = 0
        end
        shape.push(k)
        angle += piece['angle']
        pie = piece['radius']
      end
      if angle.abs > 80
          turns.push(m)
          angle = 0
      end
      m +=1 
    }
    return shape, turns
  end

  def increase_limit(piece)
    prev = piece-1 % @len
    factor = 1

    if @shape[piece] != @shape[prev] && @shape[prev] != 0
      if !@incremented && @angle_max < 25
        factor = 0.8 + (25 - @angle_max)/25

        if @pieces[prev]['radius'] < 51
          @speedLimit_1 += 10*factor
        elsif @pieces[prev]['radius'] < 101
          @speedLimit_2 += 20*factor
        elsif @pieces[prev]['radius'] < 201
          @speedLimit_3 += 30*factor
        end  

        @incremented = true
      end
      @angle_max = 0
    end
  end

  def read_info(msgData)
    speed = (msgData[0]['piecePosition']['inPieceDistance'] - @dist)*60
    speed = @old_speed if speed < 0
    @old_speed = speed

    @dist = msgData[0]['piecePosition']['inPieceDistance']
    piece = msgData[0]['piecePosition']['pieceIndex']
    angle = msgData[0]['angle']
    lap = msgData[0]['piecePosition']['lap']
    return speed.floor, piece, angle.abs, lap
  end

  def get_distance(msgData, piece, speed, lap, tcp)
    total_length = @lengths_arr[@len-1]

    next_turn = @turns[@lambda % @turns.length]
    turn_location = @lengths_arr[next_turn]

    if @gameInfo.get_pieces[next_turn]['radius'] <51
      reqSpeed = @speedLimit_1
    elsif @gameInfo.get_pieces[next_turn]['radius'] <101
      reqSpeed = @speedLimit_2
    elsif @gameInfo.get_pieces[next_turn]['radius'] <201
      reqSpeed = @speedLimit_3
    else
      reqSpeed = 600
    end

    delta = turn_location - current_location(piece) + total_length * ((@lambda/@turns.length).floor - lap)

    @lambda +=1 if delta < 0

    needed_dist = (speed - reqSpeed)*1.2

    if @turbo_on 
      needed_dist *= 2.4
      needed_dist += 100
    end

    return delta - needed_dist, delta, reqSpeed, next_turn, needed_dist
  end

  def get_lengths(pieces)
    arr = Array.new
    j = 0

    pieces.each{|piece|
      if piece.has_key?('length')
        len = piece['length']
      else
        len = piece['radius'] * 2 * Math::PI * piece['angle'].abs / 360
      end
      
      if j == 0
        arr.push(len.floor)
        j+=1
      else  
        arr.push(len.floor + arr[j-1])
        j+=1
      end
    }
    return arr
  end

  def current_location(piece)
    if piece ==0 
      return @dist
    else
      return @lengths_arr[piece-1] + @dist
    end
  end

end
  
NoobBot.new(server_host, server_port, bot_name, bot_key)
